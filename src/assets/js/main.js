function setSlickSlider(elem, param) {
	let slick = elem.slick({
		dots: param.dots,
		draggable: param.draggable,
		arrows: param.arrows,
		prevArrow: param.prevArrow,
		nextArrow: param.nextArrow,
		infinite: param.infinite,
		slidesToShow: param.slidesToShow,
		responsive: param.responsive,
		verticalSwiping: param.verticalSwiping,
		vertical: param.vertical,
		autoplay: param.autoplay,
		autoplaySpeed: param.autoplaySpeed
	});

	return slick;
}

function setSlick() {
	// setSlickSlider($('#testSlider'), {
	// 	arrows:         true,
	// 	infinite:       false,
	// 	slidesToShow:   7,
	// 	slidesToScroll: 1,
	// 	responsive:     [{
	// 		breakpoint: 1201,
	// 		settings:   {
	// 			slidesToShow:   7,
	// 			slidesToScroll: 1,
	// 		}
	// 	}, {
	// 		breakpoint: 1200,
	// 		settings:   {
	// 			slidesToShow:   6,
	// 			slidesToScroll: 1,
	// 		}
	// 	}, {
	// 		breakpoint: 1099,
	// 		settings:   {
	// 			slidesToShow:   5,
	// 			slidesToScroll: 1,
	// 		}
	// 	}, {
	// 		breakpoint: 1024,
	// 		settings:   {
	// 			slidesToShow:   4,
	// 			slidesToScroll: 1,
	// 		}
	// 	}, {
	// 		breakpoint: 800,
	// 		settings:   {
	// 			slidesToShow:   3,
	// 			slidesToScroll: 2,
	// 			arrows:         true,
	// 			dots:           true,
	// 			infinite:       true
	// 		}
	// 	},
	// 		{
	// 			breakpoint: 600,
	// 			settings:   {
	// 				slidesToShow:   2,
	// 				slidesToScroll: 2,
	// 				arrows:         true,
	// 				dots:           true,
	// 			}
	// 		},
	// 		{
	// 			breakpoint: 320,
	// 			settings:   {
	// 				slidesToShow:   1,
	// 				slidesToScroll: 1,
	// 				arrows:         true,
	// 				dots:           true,
	// 			}
	// 		}
	// 	]
	// });
}

function hamburger() {
	$('.hamburger').click(function () {
		$(this).parent().parent().toggleClass('active');
		$('body, html').toggleClass('hidden');
	});
}


$(function () {
	setSlick();
	hamburger();
})



//
window.onscroll = function () {
	scrollFunction()
};

//
var page
window.onload = function () {
	//select navbar
	var path = window.location.pathname;
	page = path.split("/").pop();
	if (page == '') {
		page = "index.html"
	}
	$('#navigation ul a[href$="' + page + '"]:first').addClass('active');

	scrollFunction()
}


function scrollFunction() {
	if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		document.getElementById("navigation").style.backgroundImage = "url('../assets/img/assett-hr6tyu-rnhnrtsyghdfg--24.png')";
		document.getElementById("navigation").style.padding = "0px 0";
	} else {
		document.getElementById("navigation").style.backgroundImage = "";
		document.getElementById("navigation").style.padding = "20px 0";
	}
}

//
function testFunction(ev){
	window.location.href = ev + page;
}

//
function downloadAnydesk(){
	var userAgent = navigator.userAgent || navigator.vendor || window.opera;

	let device = 'Web';

    if (/android/i.test(userAgent)) {
        device =  "Android";
    }
    else if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        device =  "iOS";
	}
	

	let anydeskUrl = "https://download.anydesk.com/AnyDesk.exe"
	if(device == "Android"){
		anydeskUrl = "https://play.google.com/store/apps/details?id=com.anydesk.anydeskandroid"
	}
	else if(device == "iOS"){
		anydeskUrl = "https://itunes.apple.com/us/app/anydesk/id1176131273"

	}

	window.open(anydeskUrl, '_blank');
}


$(document).ready(function () {

	//owl carousel
	var owl = $('.owl-carousel');
	owl.owlCarousel({
		// autoplay: 1000,
		autoplay: false,
		margin: 10,
		loop: true,
		autoHeight: false,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 2
			},
			1000: {
				items: 3
			}
		}
	})

	//particle
	particlesJS("particles-js", {
		"particles": {
		  "number": {
			"value": 80,
			"density": {
			  "enable": true,
			  "value_area": 800
			}
		  },
		  "color": {
			"value": "#FFFFFF"
		  },
		  "shape": {
			"type": "circle",
			"stroke": {
			  "width": 0,
			  "color": "#000000"
			},
			"polygon": {
			  "nb_sides": 3
			},
			"image": {
			  "src": "img/github.svg",
			  "width": 100,
			  "height": 100
			}
		  },
		  "opacity": {
			"value": 0.5,
			"random": false,
			"anim": {
			  "enable": false,
			  "speed": 1,
			  "opacity_min": 0.1,
			  "sync": false
			}
		  },
		  "size": {
			"value": 3,
			"random": true,
			"anim": {
			  "enable": false,
			  "speed": 90,
			  "size_min": 0.1,
			  "sync": false
			}
		  },
		  "line_linked": {
			"enable": true,
			"distance": 150,
			"color": "#FFFFFF",
			"opacity": 0.4,
			"width": 1
		  },
		  "move": {
			"enable": true,
			"speed": 6,
			"direction": "none",
			"random": false,
			"straight": false,
			"out_mode": "out",
			"bounce": false,
			"attract": {
			  "enable": false,
			  "rotateX": 600,
			  "rotateY": 1200
			}
		  }
		},
		"interactivity": {
		  "detect_on": "canvas",
		  "events": {
			"onhover": {
			  "enable": true,
			  "mode": "grab"
			},
			"onclick": {
			  "enable": true,
			  "mode": "push"
			},
			"resize": true
		  },
		  "modes": {
			"grab": {
			  "distance": 140,
			  "line_linked": {
				"opacity": 1
			  }
			},
			"bubble": {
			  "distance": 400,
			  "size": 40,
			  "duration": 2,
			  "opacity": 8,
			  "speed": 3
			},
			"repulse": {
			  "distance": 200,
			  "duration": 0.4
			},
			"push": {
			  "particles_nb": 4
			},
			"remove": {
			  "particles_nb": 2
			}
		  }
		},
		"retina_detect": true
	  });

});
